package code;
import java.util.Optional;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class CoinGame extends Application {

	private Scene scene1; //do you want to play?
	private Scene question; //first or second?
	private Scene scene2; //actual game
	private Board board;
	private static Coin[] coins;
	private static int coinsLeft; //coins left
	private static int buttonIndex; //keeps track of coins taken
	private ComputerUser computer; //the computer user

	public static void main(String [] args){
		launch(args);
	}
	
	public void start(Stage primary) throws Exception{	
		//Initialize Variables
		coins = new Coin[21];
		buttonIndex = (coins.length - 1); //starts at end of array of buttons
		board = new Board();
		coinsLeft = 21; //game starts with 21 coins
		computer = new ComputerUser();
		////////////////////////////////////////////////////////////////////////WELCOME SCREEN
		//Text and Buttons
		Label top = new Label("Welcome to the Coin Game!\n Click OK to start the game or cancel to exit!");
		top.setTextFill(Color.BLUEVIOLET);
		top.setPrefSize(250, 250);
		Button ok = new Button("OK");
		Button cancel = new Button("Cancel");
		Button rules = new Button("How to Play");
		ok.setOnAction(e -> primary.setScene(question));
		cancel.setOnAction(e -> System.exit(1));
		rules.setOnAction(e -> explain());                                               
		//Layout 
		BorderPane layout = new BorderPane();
		HBox hbox = new HBox(25);
		hbox.getChildren().addAll(ok, cancel, rules);
		VBox vbox = new VBox(25);
		vbox.getChildren().addAll(top);
		layout.setBottom(hbox);
		layout.setCenter(vbox);
		layout.setPadding(new Insets(20, 10, 10, 50));
		scene1 = new Scene(layout, 400, 200);
		//////////////////////////////////////////////////////////////////////QUESTION SCREEN
		//Text and Buttons
		Label top2 = new Label("Would you like to go first? Or second?");
		top2.setTextFill(Color.BLUEVIOLET);
		top2.setPrefSize(250, 250);
		Button first = new Button("First!");
		Button second = new Button("Second!");
		Button cancel2 = new Button("Cancel");
		first.setOnAction(e -> {
			primary.setScene(scene2);
			start();
		});
		second.setOnAction(e -> {
			primary.setScene(scene2);
			int taken = computer.go(coinsLeft);
			remove(taken);
			start();
		});
		cancel2.setOnAction(e -> System.exit(1));
		//Layout
		BorderPane layout2 = new BorderPane();
		HBox hbox2 = new HBox(25);
		hbox2.getChildren().addAll(first, second, cancel2);
		VBox vbox2 = new VBox(25);
		vbox2.getChildren().addAll(top2);
		layout2.setBottom(hbox2);
		layout2.setCenter(vbox2);
		layout2.setPadding(new Insets(20, 10, 10, 50));
		question = new Scene(layout2, 400, 200);
		////////////////////////////////////////////////////ACTUAL GAME 
		//Create Buttons
		Background fill = new Background(new BackgroundFill(Color.GRAY, new CornerRadii(50), new Insets(10, 10, 10, 10))); //grey circular background
		for(int i = 0; i < coins.length; i++){
			coins[i] = new Coin("O");
			coins[i].setBackground(fill);
			coins[i].setTextFill(Color.YELLOW);
			coins[i].setPrefSize(50, 50);
		}
		for(Coin c : coins){
			c.setOnAction(e -> {
				coinsLeft -= 1; //subtracts from number of buttons left
				buttonIndex -= 1; //moves to next button in array
				c.change(); //removes coin from button
			});
		}
		Button done1 = new Button("Done?");
		done1.setOnAction(e -> {
			if(coinsLeft == 0){
				end(2);
			}
			else{
				int taken = computer.go(coinsLeft);
				remove(taken);
			}
		});
		//Add Buttons to Board
		int index = 0;
		for(int row = 0; row < 7; row++){ 
			for(int col = 0; col < 3; col++){
				board.add(coins[index], col, row);
				++index;
			}
		}
		board.add(done1, 0, 7);
		scene2 = new Scene(board, 500, 500);
				
		//Put it All Together
		primary.setTitle("Coin Game!"); 
		primary.setScene(scene1);
		primary.show();
	}
	
	public static void start(){ ////////////////////////////////////////////START SCREEN
		Alert start = new Alert(AlertType.CONFIRMATION); 
		start.setTitle("The Game has Begun!");
		start.setHeaderText("For your turn:"); 
		start.setContentText("Please click 1 or 2 coins each turn starting from the bottom of the screen. \nWhen you are done with your turn, please press the done button.");
		start.showAndWait();
	}
	
	public static void remove(int removed){//////////////////////////////COMPUTER TURN
		if(coinsLeft == 1){ //if there's only 1 coin left, the computer lost
			end(1);
		}
		if(removed == 2){
			coins[buttonIndex].change();
			coins[buttonIndex-1].change();
		}
		else{
			coins[buttonIndex].change();
		}
		coinsLeft -= removed;
		buttonIndex -= removed;
		if(coinsLeft == 1){ //if after the computer goes 1 coin is left, the computer won
			end(2);
		}
	}

	public static void explain(){ //////////////////////////////////////RULES
		Alert explain = new Alert(AlertType.CONFIRMATION); 
		explain.setTitle("How to Play!");
		explain.setHeaderText("The Rules:"); 
		explain.setContentText("The rules are simple. You can go first or second. \nEach turn you may choose to take one or two coins.\nThe goal of the game is not to take the last coin. Have fun! ");
		explain.showAndWait();
	}
	public static void end(int winner){ //////////////////////////////END SCREEN
		Alert alert = new Alert(AlertType.CONFIRMATION); 
		if(winner == 1){
			alert.setTitle("You have won!");
			alert.setHeaderText("Congratulations on winning!"); 
		}
		else{
			alert.setTitle("You have lost!");
			alert.setHeaderText("Maybe next time..."); 
		}	
		alert.showAndWait();
		System.exit(1);
	}

}
