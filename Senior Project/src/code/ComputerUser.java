package code;

public class ComputerUser {
	
	public ComputerUser() {
		
	}
	
	public int go(int coins){ //determines what coins to take
		if(coins % 3 == 0){
			return 2;
		}
		else {
			return 1;
		}
	}
	
}
