package code;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;

public class Coin extends Button{
	
	public Coin(String text){
		super(text);
	}
	
	public void change(){
		this.setText("");
	}
}
